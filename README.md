# RPS Keycloak Extensions

Extensions that are neccessary if using RPS sync scripts or approving logic

## Development

### Getting started
- build extension locally for testing purposes:
```
cd approvedCheck && mvn clean install
```
- start local development environment:
```
docker-compose up
```
