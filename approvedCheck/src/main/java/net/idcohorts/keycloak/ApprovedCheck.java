package net.idcohorts.keycloak;

import org.jboss.logging.Logger;
import org.keycloak.authentication.AuthenticationFlowContext;
import org.keycloak.authentication.AuthenticationFlowError;
import org.keycloak.authentication.Authenticator;
import org.keycloak.forms.login.LoginFormsProvider;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.RoleModel;
import org.keycloak.models.UserModel;

import jakarta.ws.rs.core.Response;

public class ApprovedCheck implements Authenticator {
    private static final Logger logger = Logger.getLogger(ApprovedCheck.class);

    @Override
    public void close() {
        // TODO Auto-generated method stub
    }

    @Override
    public boolean requiresUser() {
        // TODO Auto-generated method stub
        return true;
    }

    public void requireApprovedRole(AuthenticationFlowContext context) {
        LoginFormsProvider form = context.form();

        form.setAttribute("actionUri", context.getActionUrl(context.generateAccessCode()));
        context.challenge(form.createLoginUsernamePassword());
        UserModel user = context.getUser();
        RoleModel approvedRole = context.getRealm().getRole("approved");
        RoleModel adminRole = context.getRealm().getRole("admin");
        if (user != null) {
            System.out.println("User is not null - requireApprovedRole");
            if ((approvedRole != null && user.hasRole(approvedRole))
                    || (adminRole != null && user.hasRole(adminRole))) {
                System.out.println("User has role approved or admin - requireApprovedRole");
                context.success();
            } else {
                System.out.println("User does not have the 'approved' role - requireApprovedRole");
                Response errorResponse = context.form()
                        .setError(
                                "Ihre Anmeldung war erfolgreich und Ihre Daten werden derzeit geprüft. Bitte haben Sie Verständnis dafür, dass dieser Prozess einige Tage in Anspruch nehmen kann. Wir werden Sie nach erfolgreicher Freischaltung informieren.")
                        .createLoginUsernamePassword();
                context.failure(AuthenticationFlowError.ACCESS_DENIED, errorResponse);
                context.getEvent().error("User does not have the 'approved' role - requireApprovedRole");
            }
        }
    }

    @Override
    public void authenticate(AuthenticationFlowContext context) {
        requireApprovedRole(context);
    }

    @Override
    public void action(AuthenticationFlowContext context) {
        requireApprovedRole(context);
    }

    @Override
    public boolean configuredFor(KeycloakSession session, RealmModel realm, UserModel user) {
        return true;
    }

    @Override
    public void setRequiredActions(KeycloakSession session, RealmModel realm, UserModel user) {

    }

}