package net.idcohorts.keycloak;

import org.jboss.logging.Logger;

import org.keycloak.authentication.AuthenticationFlowContext;
import org.keycloak.authentication.AuthenticationFlowError;
import org.keycloak.authentication.Authenticator;
import org.keycloak.authentication.actiontoken.verifyemail.VerifyEmailActionToken;
import org.keycloak.common.util.Time;
import org.keycloak.email.EmailException;
import org.keycloak.email.EmailTemplateProvider;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;
import org.keycloak.services.resources.LoginActionsService;

import jakarta.ws.rs.core.UriBuilder;

public class confirmationMail implements Authenticator {


    private static final Logger logger = Logger.getLogger(confirmationMail.class);

    private final KeycloakSession session;

    public confirmationMail(KeycloakSession session) {
        this.session = session;
    }

    @Override
    public void close() {
        // TODO Auto-generated method stub
    }

    @Override
    public void authenticate(AuthenticationFlowContext context) {
        sendConfirmationMail(context);
    }

    @Override
    public void action(AuthenticationFlowContext context) {
        sendConfirmationMail(context);
    }

    @Override
    public boolean requiresUser() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean configuredFor(KeycloakSession session, RealmModel realm, UserModel user) {
        return true;
    }

    @Override
    public void setRequiredActions(KeycloakSession session, RealmModel realm, UserModel user) {
        // TODO Auto-generated method stub
    }

    public void sendConfirmationMail(AuthenticationFlowContext context) {
        logger.info("Sending confirmation mail");
        // send confirmation mail
        UserModel user = context.getUser();
        RealmModel realm = context.getRealm();

        EmailTemplateProvider emailProvider = context.getSession().getProvider(EmailTemplateProvider.class);
        emailProvider.setRealm(realm);
        emailProvider.setUser(user);
        int expiration = Time.currentTime() + 1000000;
        // generate confirmation link
        VerifyEmailActionToken token = new VerifyEmailActionToken(
                    user.getId(),
                    expiration,
                    "oasid",
                    user.getEmail(),
                    "account-console"
            );

        UriBuilder builder = LoginActionsService.actionTokenProcessor(session.getContext().getUri());
        builder.queryParam("key", token.serialize(session, realm, session.getContext().getUri()));
        String verificationLink = builder.build(realm.getName()).toString();
        try {
            emailProvider.sendVerifyEmail(verificationLink, 100000);
            logger.info("Email sent");
            context.success();
        } catch (EmailException e) {
            logger.error("Error sending email", e);
            e.printStackTrace();
            context.failure(AuthenticationFlowError.INTERNAL_ERROR);
        }
    }
    
}
