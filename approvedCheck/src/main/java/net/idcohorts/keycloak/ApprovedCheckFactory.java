package net.idcohorts.keycloak;

import java.util.Collections;
import java.util.List;

import org.keycloak.Config.Scope;
import org.keycloak.authentication.Authenticator;
import org.keycloak.authentication.AuthenticatorFactory;
import org.keycloak.models.AuthenticationExecutionModel;
import org.keycloak.models.AuthenticationExecutionModel.Requirement;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;
import org.keycloak.provider.ProviderConfigProperty;

// import static org.keycloak.models.AuthenticationExecutionModel.Requirement.DISABLED;
import static org.keycloak.models.AuthenticationExecutionModel.Requirement.REQUIRED;

public class ApprovedCheckFactory implements AuthenticatorFactory {

    public static final String PROVIDER_ID = "requireApprovedRole";

    @Override
    public String getId() {
        return PROVIDER_ID;
    }

    @Override
    public Authenticator create(KeycloakSession session) {
        return new ApprovedCheck();
    }

    @Override
    public String getDisplayType() {
        return "requireApprovedRole";
    }

    @Override
    public String getReferenceCategory() {
        return "custom"; // This can be any string that categorizes your authenticator
    }

    @Override
    public boolean isConfigurable() {
        return true; // Change to 'false' if your authenticator is not configurable
    }

    @Override
    public Requirement[] getRequirementChoices() {
        return new AuthenticationExecutionModel.Requirement[] {
                REQUIRED
        };
    }

    @Override
    public boolean isUserSetupAllowed() {
        return false; // Change to 'true' if user setup is allowed
    }

    @Override
    public String getHelpText() {
        return "Dieser Authenticator überprüft, ob die approved-Rolle vorhanden ist, wenn sich ein User anmelden möchte."; // Provide a helpful description
    }

    @Override
    public List<ProviderConfigProperty> getConfigProperties() {
        return Collections.emptyList(); // If your authenticator is configurable, return a list of configuration
                                        // properties here
    }

    @Override
    public void init(Scope config) {
        // Initialization code
    }

    @Override
    public void postInit(KeycloakSessionFactory factory) {
        // Post-initialization code
    }

    @Override
    public void close() {
        // Cleanup code
    }
}